import datetime

from rest_framework import serializers

from emenu.models import *


class MenuSerializer(serializers.ModelSerializer):
    class Meta:
        model = Menu
        fields = "__all__"

    def validate(self, attrs):
        """
        Check that the date is not later than today.
        """
        if (
            attrs.get("date_of_addition") > datetime.date.today()
            or attrs.get("update_date") > datetime.date.today()
        ):
            raise serializers.ValidationError(
                "Date of addition and updated date must not be later than today"
            )
        if (
            attrs.get("date_of_addition") < attrs.get("update_date")
        ):
            raise serializers.ValidationError(
                "Date of addition must be earlier than updated date"
            )
        return attrs

    def update(self, instance, validated_data):
        instance.name = validated_data.get("name", instance.name)
        instance.description = validated_data.get("description", instance.description)
        instance.date_of_addition = validated_data.get(
            "date_of_addition", instance.date_of_addition
        )
        instance.update_date = validated_data.get("update_date", instance.update_date)
        dishes = validated_data.pop("dishes", [])
        for dish in instance.dishes.all():
            instance.dishes.remove(dish)
        for dish in dishes:
            instance.dishes.add(dish)
        instance.save()
        return instance


class MenuGetDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Menu
        fields = "__all__"
        depth = 1


class MenuGetFilteredSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=255, required=False)
    update_date_from = serializers.DateField(required=False)
    update_date_to = serializers.DateField(required=False)
    date_of_addition_from = serializers.DateField(required=False)
    date_of_addition_to = serializers.DateField(required=False)


class DishSerializer(serializers.ModelSerializer):
    class Meta:
        model = Dish
        fields = "__all__"

    def validate(self, attrs):
        """
        Check that the date is not later than today.
        """
        if (
            attrs.get("date_of_addition") > datetime.date.today()
            or attrs.get("update_date") > datetime.date.today()
        ):
            raise serializers.ValidationError(
                "Date of addition and updated date must not be later than today"
            )
        if (
            attrs.get("date_of_addition") < attrs.get("update_date")
        ):
            raise serializers.ValidationError(
                "Date of addition must be earlier than updated date"
            )
        return attrs

    def update(self, instance, validated_data):
        instance.name = validated_data.get("name", instance.name)
        instance.description = validated_data.get("description", instance.description)
        instance.price = validated_data.get("price", instance.price)
        instance.preparation_time = validated_data.get(
            "preparation_time", instance.preparation_time
        )
        instance.date_of_addition = validated_data.get(
            "date_of_addition", instance.date_of_addition
        )
        instance.update_date = validated_data.get("update_date", instance.update_date)
        instance.vegetarian_dish = validated_data.get(
            "vegetarian_dish", instance.vegetarian_dish
        )
        instance.save()
        return instance


class ChoiceOrderSerializer(serializers.Serializer):
    choice = serializers.ChoiceField(
        [
            "name_descending",
            "name_ascending",
            "number_of_dishes_descending",
            "number_of_dishes_ascending",
        ],
        required=False,
    )
