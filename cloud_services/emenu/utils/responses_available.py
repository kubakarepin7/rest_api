from emenu.serializers import *


response_dish_id_list = {
    200: "Id list of all dishes",
    401: "Error: Unauthorized",
    404: "Error: Not found",
}

response_create_dish = {
    201: DishSerializer,
    401: "Error: Unauthorized",
    400: "Error: Bad request",
}

response_get_dish = {
    200: DishSerializer,
    401: "Error: Unauthorized",
    404: "Error: Not found",
}

response_update_dish = {
    200: DishSerializer,
    401: "Error: Unauthorized",
    404: "Error: Not found",
}

response_delete_dish = {
    204: "Object with the given id {pk} has been deleted",
    401: "Error: Unauthorized",
    404: "Error: Not found",
}

response_menu_name_list = {200: "Name list of all menu cards", 404: "Error: Not found"}

response_create_menu = {
    201: MenuSerializer,
    401: "Error: Unauthorized",
    400: "Error: Bad request",
}

response_get_menu = {200: MenuGetDetailSerializer, 404: "Error: Not found"}

response_get_menu_ordered = {200: MenuSerializer, 404: "Error: Not found"}

response_get_menu_filtered = {200: MenuSerializer, 404: "Error: Not found"}

response_update_menu = {
    200: MenuSerializer,
    401: "Error: Unauthorized",
    404: "Error: Not found",
}

response_delete_menu = {
    204: "Object with the given name {name} has been deleted",
    401: "Error: Unauthorized",
    404: "Error: Not found",
}
