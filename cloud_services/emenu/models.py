from django.db import models
from datetime import date


class Dish(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField()
    price = models.FloatField()
    preparation_time = models.PositiveSmallIntegerField(
        help_text="Time given in minutes"
    )
    date_of_addition = models.DateField(help_text="Date format: yyyy-MM-dd")
    update_date = models.DateField(help_text="Date format: yyyy-MM-dd")
    vegetarian_dish = models.BooleanField()


class Menu(models.Model):
    name = models.CharField(unique=True, max_length=255)
    description = models.TextField()
    date_of_addition = models.DateField(help_text="Date format: yyyy-MM-dd")
    update_date = models.DateField(help_text="Date format: yyyy-MM-dd")
    dishes = models.ManyToManyField(Dish, blank=True)
