from django.urls import path, include
from rest_framework.routers import DefaultRouter
from emenu.views import (
    MenuManagementPrivateView,
    DishManagementView,
    MenuManagementPublicView,
)

# Create a router and register our viewsets with it.
router = DefaultRouter()
router.register(r"menu", MenuManagementPrivateView, basename="menu")
router.register(r"dish", DishManagementView, basename="dish")
router.register(r"menu_public", MenuManagementPublicView, basename="menu_public")

# The API URLs are now determined automatically by the router.
urlpatterns = [
    path("", include(router.urls)),
]
