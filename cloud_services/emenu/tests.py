from rest_framework.test import APITestCase, APIRequestFactory
from django.urls import reverse
from emenu.views import *
from user_management.views import *


class MenuPublicTestCase(APITestCase):
    def setUp(self):
        self.factory = APIRequestFactory()
        self.dish = Dish.objects.create(name="Pizza", description="Pizza dish", preparation_time=30, price=20,
                                        date_of_addition="2022-10-10", update_date="2022-10-10", vegetarian_dish=False)
        self.dish_two = Dish.objects.create(name="Pasta", description="Pasta dish", preparation_time=30, price=20,
                                        date_of_addition="2022-10-10", update_date="2022-10-10", vegetarian_dish=False)
        self.menu = Menu.objects.create(name="Menu", description="New menu card", date_of_addition="2023-02-20",
                                        update_date="2023-01-25")
        self.menu_two = Menu.objects.create(name="Test Menu", description="New menu card", date_of_addition="2023-01-20",
                                        update_date="2023-02-25")
        self.menu.dishes.add(self.dish, self.dish_two)
        self.menu_two.dishes.add(self.dish)
        self.menus = Menu.objects.all()

    def test_list(self):
        url = reverse("menu_public-list")
        request = self.factory.get(url)
        view = MenuManagementPublicView.as_view({"get": "list"})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_retrieve(self):
        view = MenuManagementPublicView.as_view({"get": "retrieve"})
        request = self.factory.get("")
        response = view(request, name=self.menu_two.name)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_ordered_menu_cards(self):
        view = MenuManagementPublicView.as_view(actions={"get": "get_ordered_menu_cards"})
        choice = ["name_descending", "name_ascending", "number_of_dishes_descending", "number_of_dishes_ascending"]
        for i in choice:
            request = self.factory.get("", {"choice": i})
            response = view(request)
            self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_filtered_menu_cards(self):
        view = MenuManagementPublicView.as_view(actions={"get": "get_filtered_menu_cards"})
        name = ["Menu", "Te"]
        update_date_from = ["2023-2-10", "2023-1-26"]
        update_date_to = ["2023-3-01", "2023-2-05"]
        date_of_addition_from = ["2023-2-10", "2023-1-26"]
        date_of_addition_to = ["2023-3-01", "2023-2-05"]
        for i in range(len(name)):
            request = self.factory.get("", {"name": name[i],
                                            "update_date_from": update_date_from[i],
                                            "update_date_to": update_date_to[i],
                                            "date_of_addition_from": date_of_addition_from[i],
                                            "date_of_addition_to": date_of_addition_to[i]})
            response = view(request)
            self.assertEqual(response.status_code, status.HTTP_200_OK)


class MenuPrivateTestCase(APITestCase):
    def setUp(self):
        self.factory = APIRequestFactory()
        self.dish = Dish.objects.create(name="Pizza", description="Pizza dish", preparation_time=30, price=20,
                                        date_of_addition="2022-10-10", update_date="2022-10-10", vegetarian_dish=False)
        self.menu = Menu.objects.create(name="Menu", description="New menu card", date_of_addition="2023-02-20",
                                        update_date="2023-01-25")
        self.menu.dishes.add(self.dish)
        view_register = UserRegistrationView.as_view()
        request = self.factory.post("register", {"email": "user@example.com", "password": "string", "password2": "string",
                                               "date_of_birth": "1996-10-10"})
        view_register(request)
        view_login = UserLoginView.as_view()
        request = self.factory.post("login", {"email": "user@example.com", "password": "string"})
        response = view_login(request)
        self.token = response.data.get('token').get('access')

    def test_create_menu(self):
        view = MenuManagementPrivateView.as_view({"post": "create"})
        sample_data = {"name": "Testing_menu", "description": "New menu card", "date_of_addition": "2023-02-20",
                                        "update_date": "2023-01-25"}
        request = self.factory.post("", sample_data, **{ "HTTP_AUTHORIZATION": f"Bearer {self.token}" })
        response = view(request)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data["name"], sample_data["name"])

    def test_get_all_menu_cards(self):
        url = reverse("menu-list")
        view = MenuManagementPrivateView.as_view({"get": "list"})
        request = self.factory.get(url,  **{ "HTTP_AUTHORIZATION": f"Bearer {self.token}" })
        response = view(request)

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_delete_menu_cards_names(self):
        view = MenuManagementPrivateView.as_view({"delete": "destroy"})
        request = self.factory.delete("", **{ "HTTP_AUTHORIZATION": f"Bearer {self.token}" })
        response = view(request, name="Menu")

        self.assertEqual(response.status_code, status.HTTP_200_OK)


class DishPrivateTestCase(APITestCase):
    def setUp(self):
        self.factory = APIRequestFactory()
        self.dish = Dish.objects.create(name="Pizza", description="Pizza dish", preparation_time=30, price=20,
                                        date_of_addition="2022-10-10", update_date="2022-10-10", vegetarian_dish=False)
        view_register = UserRegistrationView.as_view()
        request = self.factory.post("register", {"email": "user@example.com", "password": "string", "password2": "string",
                                               "date_of_birth": "1996-10-10"})
        view_register(request)
        view_login = UserLoginView.as_view()
        request = self.factory.post("login", {"email": "user@example.com", "password": "string"})
        response = view_login(request)
        self.token = response.data.get('token').get('access')

    def test_get_all_dishes_names(self):
        url = reverse("dish-list")
        view = DishManagementView.as_view({"get": "list"})
        request = self.factory.get(url,  **{ "HTTP_AUTHORIZATION": f"Bearer {self.token}" })
        response = view(request)

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_delete_menu_card(self):
        view = DishManagementView.as_view({"delete": "destroy"})
        request = self.factory.delete("", **{ "HTTP_AUTHORIZATION": f"Bearer {self.token}" })
        response = view(request, pk=1)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
