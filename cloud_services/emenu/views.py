from django.db.models import Count
from django.shortcuts import get_object_or_404
from drf_yasg.utils import swagger_auto_schema
from rest_framework import status, viewsets
from rest_framework.decorators import action
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from emenu.filters import FilterBackend
from emenu.utils.responses_available import *


class MenuManagementPublicView(viewsets.ViewSet):
    permission_classes = [AllowAny]
    serializer_class = MenuSerializer
    lookup_field = "name"

    @swagger_auto_schema(
        operation_summary="Get a list of names of all non-empty menu cards",
        tags=["Menu management public"],
        responses=response_menu_name_list,
    )
    def list(self, request):
        try:
            queryset = list(
                Menu.objects.filter(dishes__isnull=False).values_list("name", flat=True)
            )
            return Response(queryset, status=status.HTTP_200_OK)
        except (Exception,):
            return Response(dict(name=None), status=status.HTTP_404_NOT_FOUND)

    @swagger_auto_schema(
        operation_summary="Get detailed information about a non-empty menu card based on its name",
        tags=["Menu management public"],
        responses=response_get_menu,
    )
    def retrieve(self, request, *args, **kwargs):
        name = kwargs.get("name")
        try:
            menu_object = Menu.objects.filter(dishes__isnull=False).get(name=name)
            serializer = MenuGetDetailSerializer(menu_object)
            return Response(data=serializer.data, status=status.HTTP_200_OK)
        except (Exception,):
            return Response(
                {f"The object with the given name: {name} does not exist"},
                status=status.HTTP_404_NOT_FOUND,
            )

    @swagger_auto_schema(
        operation_summary="Get a list of all non-empty menu cards ordered by name and number of dishes",
        tags=["Menu management public"],
        query_serializer=ChoiceOrderSerializer(),
        responses=response_get_menu_ordered,
    )
    @action(
        methods=["GET"],
        detail=False,
        url_name="get_ordered_menu_cards",
        url_path="get_ordered_menu_cards",
    )
    def get_ordered_menu_cards(self, request):
        order_by = self.request.query_params.get("choice")
        choice_dict = {
            "name_descending": "-name",
            "name_ascending": "name",
            "number_of_dishes_descending": "-dishes_count",
            "number_of_dishes_ascending": "dishes_count",
        }
        if order_by:
            if (
                order_by == "number_of_dishes_descending"
                or order_by == "number_of_dishes_ascending"
            ):
                menu_object = (
                    Menu.objects.all()
                    .filter(dishes__isnull=False)
                    .annotate(dishes_count=Count("dishes"))
                    .order_by(choice_dict.get(order_by), "name")
                )
            else:
                menu_object = (
                    Menu.objects.all()
                    .filter(dishes__isnull=False)
                    .order_by(choice_dict.get(order_by))
                )
        else:
            menu_object = Menu.objects.all().filter(dishes__isnull=False)
        serializer = MenuSerializer(menu_object, many=True)
        return Response(data=serializer.data, status=status.HTTP_200_OK)

    @swagger_auto_schema(
        operation_summary="Get a list of all non-empty menu cards filtered by name, "
        "date of addition and update date",
        tags=["Menu management public"],
        query_serializer=MenuGetFilteredSerializer(),
    )
    @action(
        methods=["GET"],
        detail=False,
        url_name="get_filtered_menu_cards",
        url_path="get_filtered_menu_cards",
    )
    def get_filtered_menu_cards(self, request):
        filter_backends = FilterBackend()
        queryset = filter_backends.filter_queryset(

            self.request.query_params, queryset=Menu.objects.all()
        )
        serializer = MenuSerializer(queryset, many=True)
        return Response(data=serializer.data, status=status.HTTP_200_OK)


class MenuManagementPrivateView(viewsets.ViewSet):
    permission_classes = [IsAuthenticated]
    serializer_class = MenuSerializer
    lookup_field = "name"

    @swagger_auto_schema(
        operation_summary="Get a list of names of all menu cards",
        tags=["Menu management private"],
        responses=response_menu_name_list,
    )
    def list(self, request):
        try:
            queryset = list(Menu.objects.values_list("name", flat=True))
            return Response(queryset, status=status.HTTP_200_OK)
        except (Exception,):
            return Response(dict(name=None), status=status.HTTP_404_NOT_FOUND)

    @swagger_auto_schema(
        operation_summary="Get detailed information about a menu card based on its name",
        tags=["Menu management private"],
        responses=response_get_menu,
    )
    def retrieve(self, request, *args, **kwargs):
        name = kwargs.get("name")
        try:
            menu_object = Menu.objects.get(name=name)
            serializer = MenuGetDetailSerializer(menu_object)
            return Response(data=serializer.data, status=status.HTTP_200_OK)
        except (Exception,):
            return Response(
                {f"The object with the given name: {name} does not exist"},
                status=status.HTTP_404_NOT_FOUND,
            )

    @swagger_auto_schema(
        operation_summary="Get a list of all menu cards",
        tags=["Menu management private"],
        responses=response_get_menu_ordered,
    )
    @action(
        methods=["GET"],
        detail=False,
        url_name="get_all_menu_cards",
        url_path="get_all_menu_cards",
    )
    def get_all_menu_cards(self, request):
        # Possible menu_object = Menu.objects.all().iterator(chunk_size=value) once measured, this is not optimal
        menu_object = Menu.objects.all()
        serializer = MenuSerializer(menu_object, many=True)
        return Response(data=serializer.data, status=status.HTTP_200_OK)

    @swagger_auto_schema(
        operation_summary="Create new menu",
        tags=["Menu management private"],
        request_body=MenuSerializer(),
        responses=response_create_menu,
    )
    def create(self, request):
        serializer = MenuSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(data=serializer.data, status=status.HTTP_201_CREATED)

    @swagger_auto_schema(
        operation_summary="Update menu with the given name",
        tags=["Menu management private"],
        request_body=MenuSerializer,
        responses=response_update_menu,
    )
    def update(self, request, *args, **kwargs):
        name = kwargs.get("name")
        try:
            menu_object = Menu.objects.get(name=name)
            serializer = MenuSerializer(menu_object, data=request.data)
            serializer.update(instance=menu_object, validated_data=request.data)
            return Response(data=serializer.initial_data, status=status.HTTP_200_OK)
        except (Exception,):
            return Response(
                {f"The object with the given name: {name} does not exist"},
                status=status.HTTP_404_NOT_FOUND,
            )

    @swagger_auto_schema(
        operation_summary="Delete a menu based on its name",
        tags=["Menu management private"],
        responses=response_delete_menu,
    )
    def destroy(self, request, *args, **kwargs):
        name = kwargs.get("name")
        menu_object = get_object_or_404(Menu, name=name)
        menu_object.delete()
        return Response(
            f"Object with the given name {name} has been deleted",
            status=status.HTTP_200_OK,
        )


class DishManagementView(viewsets.ViewSet):
    permission_classes = [IsAuthenticated]
    serializer_class = DishSerializer

    @swagger_auto_schema(
        operation_summary="Get the id list of all dishes",
        tags=["Dish management private"],
        responses=response_dish_id_list,
    )
    def list(self, request):
        try:
            queryset_data = list(Dish.objects.values_list("id", flat=True))
            return Response(queryset_data, status=status.HTTP_200_OK)
        except (Exception,):
            return Response(dict(name=None), status=status.HTTP_404_NOT_FOUND)

    @swagger_auto_schema(
        operation_summary="Create new dish",
        tags=["Dish management private"],
        request_body=DishSerializer(),
        responses=response_create_dish,
    )
    def create(self, request):
        serializer = DishSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(data=serializer.data, status=status.HTTP_201_CREATED)

    @swagger_auto_schema(
        operation_summary="Get information about a dish based on its id",
        tags=["Dish management private"],
        responses=response_get_dish,
    )
    def retrieve(self, request, pk=None):
        dish_object = get_object_or_404(Dish, pk=pk)
        serializer = DishSerializer(dish_object)
        return Response(data=serializer.data, status=status.HTTP_200_OK)

    @swagger_auto_schema(
        operation_summary="Update dish with the given id",
        tags=["Dish management private"],
        request_body=DishSerializer,
        responses=response_update_dish,
    )
    def update(self, request, pk=None):
        dish_object = get_object_or_404(Dish, pk=pk)
        serializer = DishSerializer(dish_object, data=request.data)
        serializer.update(instance=dish_object, validated_data=request.data)
        return Response(data=serializer.initial_data, status=status.HTTP_200_OK)

    @swagger_auto_schema(
        operation_summary="Delete a dish based on its id",
        tags=["Dish management private"],
        responses=response_delete_dish,
    )
    def destroy(self, request, pk=None):
        dish_object = get_object_or_404(Dish, pk=pk)
        dish_object.delete()
        return Response(
            f"Object with the given id {pk} has been deleted", status=status.HTTP_200_OK
        )
