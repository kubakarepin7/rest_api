import datetime
from datetime import timedelta

from celery import shared_task
from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template

from emenu.models import Dish
from user_management.models import MyUser


@shared_task()
def schedule_mail():
    dishes_update = (
        Dish.objects.filter(update_date__gte=datetime.date.today() - timedelta(1))
        .values(
            "name",
            "description",
            "price",
            "preparation_time",
            "vegetarian_dish",
            "date_of_addition",
            "update_date",
        )
        .distinct()
    )
    dishes_added = (
        Dish.objects.filter(date_of_addition__gte=datetime.date.today() - timedelta(1))
        .values(
            "name",
            "description",
            "price",
            "preparation_time",
            "vegetarian_dish",
            "date_of_addition",
            "update_date",
        )
        .distinct()
    )
    queryset = dishes_added | dishes_update
    context = {"object_list": queryset}
    html_template = get_template("schedule_mail.html")
    message = html_template.render(context)
    mail_subject = "Emenu new and modified dishes"
    recipient_list = MyUser.objects.all()
    email = EmailMultiAlternatives(
        mail_subject, message, to=[r.email for r in recipient_list]
    )
    email.attach_alternative(message, "text/html")
    email.send()
