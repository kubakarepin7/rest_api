from rest_framework import filters


class FilterBackend(filters.BaseFilterBackend):
    def filter_queryset(self, request, queryset, *args, **kwargs):
        if request.get("name", None) is not None:
            queryset = queryset.filter(name__istartswith=request["name"])
        if (
            request.get("date_of_addition_from", None) is not None
            and request.get("date_of_addition_to", None) is not None
        ):
            queryset = queryset.filter(
                date_of_addition__range=(
                    request["date_of_addition_from"],
                    request["date_of_addition_to"],
                )
            )
        if (
            request.get("date_of_addition_from", None) is None
            and request.get("date_of_addition_to", None) is not None
        ):
            queryset = queryset.filter(
                date_of_addition__lte=request["date_of_addition_to"]
            )
        if (
            request.get("date_of_addition_from", None) is not None
            and request.get("date_of_addition_to", None) is None
        ):
            queryset = queryset.filter(
                date_of_addition__gte=request["date_of_addition_from"]
            )
        if (
            request.get("update_date_from", None) is not None
            and request.get("update_date_to", None) is not None
        ):
            queryset = queryset.filter(
                update_date__range=(
                    request["update_date_from"],
                    request["update_date_to"],
                )
            )
        if (
            request.get("update_date_from", None) is None
            and request.get("update_date_to", None) is not None
        ):
            queryset = queryset.filter(update_date__lte=request["update_date_to"])
        if (
            request.get("update_date_from", None) is not None
            and request.get("update_date_to", None) is None
        ):
            queryset = queryset.filter(update_date__gte=request["update_date_from"])
        return queryset
