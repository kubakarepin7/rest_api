from django.urls import path

from user_management.views import (
    UserRegistrationView,
    UserLoginView,
    UserProfileView,
    DecoratedTokenRefreshView,
    DecoratedTokenVerifyView,
)

urlpatterns = [
    path("register/", UserRegistrationView.as_view(), name="register"),
    path("login/", UserLoginView.as_view(), name="login"),
    path("profile/", UserProfileView.as_view(), name="profile"),
    path("token/refresh/", DecoratedTokenRefreshView.as_view(), name="token_refresh"),
    path("token/verify/", DecoratedTokenVerifyView.as_view(), name="token_verify"),
]
