from django.contrib.auth import authenticate
from django.contrib.auth.models import update_last_login
from drf_yasg.utils import swagger_auto_schema
from rest_framework import status
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework_simplejwt.views import TokenVerifyView, TokenRefreshView

from user_management.renderers import UserRenderer
from user_management.serializers import (
    UserRegistrationSerializer,
    UserLoginSerializer,
    TokenRefreshResponseSerializer,
    UserProfileSerializer,
    TokenVerifyResponseSerializer,
)


def get_tokens_for_user(user):
    refresh = RefreshToken.for_user(user)
    return {
        "refresh": str(refresh),
        "access": str(refresh.access_token),
    }


class UserRegistrationView(APIView):
    renderer_classes = [UserRenderer]
    permission_classes = [AllowAny]

    @swagger_auto_schema(
        operation_summary="Registration",
        tags=["Authentication"],
        request_body=UserRegistrationSerializer,
    )
    def post(self, request):
        """
        Sign up.
        """
        serializer = UserRegistrationSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.save()
        token = get_tokens_for_user(user)
        return Response(
            {"token": token, "msg": "Registration Successful"},
            status=status.HTTP_201_CREATED,
        )


class UserLoginView(APIView):
    renderer_classes = [UserRenderer]
    permission_classes = [AllowAny]

    @swagger_auto_schema(
        operation_summary="Login",
        tags=["Authentication"],
        request_body=UserLoginSerializer,
        responses={status.HTTP_200_OK: TokenRefreshResponseSerializer()},
    )
    def post(self, request):
        """
        Sign in.
        """
        serializer = UserLoginSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        email = serializer.data.get("email")
        password = serializer.data.get("password")
        user = authenticate(email=email, password=password)
        if user is not None:
            update_last_login(None, user)
            token = get_tokens_for_user(user)
            return Response({"token": token}, status=status.HTTP_200_OK)
        else:
            return Response(
                {"errors": {"non_field_errors": ["Email or Password is not Valid"]}},
                status=status.HTTP_404_NOT_FOUND,
            )


class UserProfileView(APIView):
    renderer_classes = [UserRenderer]
    permission_classes = [IsAuthenticated]

    @swagger_auto_schema(
        operation_summary="Profile",
        tags=["Authentication"],
        query_serializer=UserProfileSerializer(),
    )
    def get(self, request):
        """
        Get user's details.
        """
        return Response(request.user.get_user_info(), status=status.HTTP_200_OK)


class DecoratedTokenVerifyView(TokenVerifyView):
    @swagger_auto_schema(
        operation_summary="Refresh token validation",
        tags=["Authentication"],
        responses={status.HTTP_200_OK: TokenVerifyResponseSerializer()},
    )
    def post(self, request, *args, **kwargs):
        """
        Takes a token and indicates if it is valid.
        This view provides no information about a token’s fitness for a particular use.
        """
        return super().post(request, *args, **kwargs)


class DecoratedTokenRefreshView(TokenRefreshView):
    @swagger_auto_schema(
        operation_summary="Return access JWT",
        tags=["Authentication"],
        responses={status.HTTP_200_OK: TokenRefreshResponseSerializer()},
    )
    def post(self, request, *args, **kwargs):
        """
        Takes a refresh type JSON web token and returns an access type JSON web token if the refresh token is valid.
        """
        return super().post(request, *args, **kwargs)
